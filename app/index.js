import React, { Component } from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import Home from './screens/Home';
import { Font, AppLoading } from 'expo'; 

EStyleSheet.build({
    $white: '#FFFFFF',
    $pinkish_tan: '#e09186'
});


export default class App extends Component {

    constructor() {
        super();
        this.state = {
            fontLoaded: false
        };
    }

    async loadFont() {
        await Font.loadAsync({
          'Ubuntu-Light': require('./fonts/Ubuntu-Light.ttf'),
          'Ubuntu-Medium': require('./fonts/Ubuntu-Medium.ttf'),
          'Ubuntu-Italic': require('./fonts/Ubuntu-Italic.ttf'),
        });
      }

    render() {
        if(!this.state.fontLoaded)
        {
            return (
                <AppLoading
                    startAsync={this.loadFont}
                    onFinish={() => this.setState({ fontLoaded: true }) }
                    onError={console.warn}
                />
            );
        }

        return (
            <Home/>
        );
    }
}