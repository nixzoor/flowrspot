import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    logoText:{
        fontFamily:'Ubuntu-Medium',
        color: '$pinkish_tan',
        fontSize: 20
    },
});