import React from 'react';
import { View, Image, Text } from 'react-native';

import styles from './styles';

const Logo = () => (
    <View style={styles.container}>
        <Image style={styles.logo}/>
        <Text style={styles.logoText}>
            FlowrSpot
        </Text>
    </View>
);

export default Logo; 