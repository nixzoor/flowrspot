import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    backgorundImage:{
        alignSelf: 'stretch',
        height: 315,
        alignItems: 'center',
        paddingHorizontal: 20
    },
    title:{
        fontFamily:'Ubuntu-Light',
        fontSize:24,
        color:'$white',
        paddingTop: 61,
        textAlign:'center'
    },
    subtitle:{
        fontFamily:'Ubuntu-Light',
        fontSize:15,
        color:'$white',
        paddingVertical:26
    }

})