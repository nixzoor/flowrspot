import React from 'react';
import { View, Text, ImageBackground } from 'react-native';

import styles from './styles';
import { SearchInput } from '../TextInput';

const SearchTile = ({sightseeingNum, onSearch, onChangeText}) => {
    return (
        <ImageBackground style={styles.backgorundImage} source={require('./images/pl_hero.png')}>
                <Text style={styles.title}>Discover flowers{"\n"} around you</Text>
                <Text style={styles.subtitle}>Explore between more than {sightseeingNum} sightseeing</Text>
                <SearchInput placeholder="Looking for something specific?" onConfirm={onSearch} onChangeText={onChangeText}/>
        </ImageBackground>
    );
}

export default SearchTile;