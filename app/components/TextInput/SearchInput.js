import React from 'react';
import { View, TouchableOpacity , TextInput, Image } from 'react-native';

import styles from './styles';

export default SearchInput = ({onConfirm, ...props}) => (
    <View style={styles.container}>
        <TextInput style={styles.input} {...props} underlineColorAndroid="transparent"/>
        <TouchableOpacity  style={styles.touchableContainer} onPress={onConfirm} >
            <Image resizeMode="contain" style={styles.icon} source={require('./images/pl_icon_search.png')}/>
        </TouchableOpacity >
    </View>
)