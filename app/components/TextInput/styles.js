import EStyleSheet from 'react-native-extended-stylesheet';

const BORDER_RADIUS = 4;
const ICON_HEIGHT = 20;
const CONTAINER_HEIGHT = 48;

export default styles = EStyleSheet.create({
    container: {
      height: CONTAINER_HEIGHT,
      alignSelf: 'stretch',
      backgroundColor: '$white',
      flexDirection: 'row',
      alignItems: 'center',    
      borderRadius: BORDER_RADIUS
    },
    input: {
        fontFamily:'Ubuntu-Light',
        flex: 1,
        fontSize: 14,
        paddingLeft: 20,
        borderTopLeftRadius: BORDER_RADIUS,
        borderBottomLeftRadius: BORDER_RADIUS
    },
    touchableContainer:{
        borderTopRightRadius: BORDER_RADIUS,
        borderBottomRightRadius: BORDER_RADIUS,
        height:CONTAINER_HEIGHT,
        width:CONTAINER_HEIGHT,
        alignItems:'center',
        justifyContent:'center'
    },
    icon:{
        height:ICON_HEIGHT,
        width:ICON_HEIGHT
    }
  });