import EStyleSheet from "react-native-extended-stylesheet";
import { Dimensions } from "react-native";

const LIST_PADDING = 12;
const WIDTH = Dimensions.get('window').width / 2 - (LIST_PADDING+LIST_PADDING/2);

export default EStyleSheet.create({
    $radius:2.4,
    $black:'#80000000',

    gradient:{
        flex:1, 
        alignItems: 'center', 
        alignSelf:'stretch' , 
        borderRadius: 5, 
        backgroundColor:'transparent' 
    },
    listContainer:{
        flexDirection: 'row',
        flexWrap: 'wrap',
        padding:LIST_PADDING,
        justifyContent: 'space-between'
    },
    listItem:{
        width:WIDTH,
        height: 200,
        alignItems:'center',
        borderRadius:'$radius',
        marginBottom: LIST_PADDING,
    },
    textContainer:{
        position:'absolute',
        bottom:0,
        alignItems:'center',
        marginBottom:16
    },
    flowerName:{
        marginBottom:3,
        fontFamily:'Ubuntu-Light',
        fontSize:16,
        color:'$white',
    },
    latinName:{
        fontFamily:'Ubuntu-Italic',
        fontSize:10,
        color:'$white',
        fontStyle:'italic',
        marginBottom:15,
        opacity:0.7,
    },
    sightingsBackground:{
        padding:5,
        backgroundColor: 'rgba(0,0,0,0.5)',
        borderRadius:20,
        alignItems:'center'
    },
    sightings:{
        fontFamily:'Ubuntu-Light',
        fontSize:10,
        color:'$white',       
    },
});