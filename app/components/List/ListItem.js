import React from 'react';
import { ImageBackground, View, Text } from 'react-native';
import { LinearGradient } from 'expo';

import styles from './styles';

const ListItem = ({name,latinname,sightings, profile_picture}) => (
    
        <ImageBackground source={{uri: 'http:' + profile_picture}} style={styles.listItem}  imageStyle={{ borderRadius: styles.$radius }} >
            <LinearGradient
                colors={['rgba(0, 0, 0,0)', 'rgba(0, 0, 0,0.2)', 'rgb(0, 0, 0)']}
                style={styles.gradient}>
                <View style={styles.textContainer}>
                    <Text style={styles.flowerName}>{name}</Text>
                    <Text style={styles.latinName}>{latinname}</Text>
                    <View style={styles.sightingsBackground}>
                        <Text style={styles.sightings}>{sightings} sightings</Text>
                    </View>
                </View>         
            </LinearGradient>
        </ImageBackground>
);

export default ListItem;