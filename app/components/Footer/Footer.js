import React from 'react';
import { View, Image } from 'react-native';

import styles from './styles';

const Footer = () => (
    <View style={styles.container}>
        <Image resizeMode="contain" style={styles.icon} source={require('./images/favorites_icons.png')}/>
        <Image resizeMode="contain" style={styles.icon} source={require('./images/comment_icon.png')}/>
        <Image resizeMode="contain" style={styles.icon} source={require('./images/new_sighting_icon.png')}/>
        <Image resizeMode="contain" style={styles.icon} source={require('./images/sighting_list_icon.png')}/>
    </View>
)

export default Footer;