import EStyleSheet from 'react-native-extended-stylesheet';

const ICON_WIDTH = 29;

export default EStyleSheet.create({
    container:{
        width:'100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal:35,
        paddingVertical:10
    },
    icon:{
        width:ICON_WIDTH,
        height:ICON_WIDTH,
    }
});