import React from 'react';
import {Text,Image,View} from 'react-native';
import { Logo } from '../Logo';

import styles from  './styles';

export default Header = () => (
    <View style={styles.container}>
        <Image/>
        <Logo/>
    </View>
);