import EStyleSheet from 'react-native-extended-stylesheet';
import { StatusBar } from 'react-native';

export default styles = EStyleSheet.create({
    container: {
      alignItems:'center',
      justifyContent:'center',
      backgroundColor:'$white',
      width:'100%',
      height: 45,
      '@media android':{
        marginTop: StatusBar.currentHeight,
      },
      '@media ios': {
        marginTop:20
      }
    },
  });