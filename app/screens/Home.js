
import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';

import { Container } from '../components/Container';
import { SearchTile } from '../components/SearchTile';
import { ListItem } from '../components/List';
import { Header } from '../components/Header';
import { Footer } from "../components/Footer";

import dummy from '../data/dummy';

import styles from '../components/List/styles';

const AUTHORIZATION_KEY = 'eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1MDQ4NjA2Mjh9.XNVBzeLpr6gRlu2r_yXXEvyzD_SKMoqAkvo7rW_PrGc';

export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      flowers:[],
      searchText:'',
    }
    this.searchQuery();
  }

  searchQuery = () => {
      fetch('https://flowrspot-api.herokuapp.com/api/v1/flowers/search?query=' + this.state.searchText, {
        method: 'GET',
        headers: {
          Authorization: AUTHORIZATION_KEY,
        },
      })
      .then((response) => response.json())
      .then((queryResult) => {
        this.setState({flowers: queryResult.flowers});
      })
      .catch((error) => {
        console.error(error); 
      });;
  }

    render() {
      return (
        <Container>
          <Header/>
          <ScrollView style={{alignSelf:'stretch'}}>
            <SearchTile onSearch={this.searchQuery} onChangeText={(text) => this.setState({searchText:text})}/>
            <View style={styles.listContainer}>
              {
                  this.state.flowers.map((item) => (
                    <ListItem key={item.id} name={item.name} latinname={item.latin_name} sightings={item.sightings} profile_picture={item.profile_picture}/>
                  ))
              }
            </View>
            </ScrollView>
          <Footer/>
        </Container>
      );
    }
  }
  
  